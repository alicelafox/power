#Power Consumption Demo
by Alice Lafox (alice@lafox.net)

##Run it with: 
    mvn spring-boot:run
    
##Test it here: 
    http://localhost:8080
   
#### Or run on another port, if 8080 is used:       
    mvn spring-boot:run -Dserver.port=8090
    
 
## REST API for Profiles/Fractions 
       GET /api/v1/profile/{profileName} - returns fractions for given profile
       
       POST /api/v1/profile - creates profiles/fractions 
       
       DELETE /api/v1/profile/{profileName}
      
       PUT /api/v1/profile - update not implemented
        
## REST API for Meter Readings  
     
       GET /api/v1/meterReading/{connectionId} - returns meter readings for given connectionId
       
       POST /api/v1/meterReading - creates meter readings 
       
       DELETE /api/v1/meterReading/{connectionId}
      
       PUT /api/v1/meterReading - update not implemented
    
## REST API for Consumption
    
     GET  /api/v1/consumption/{month}  - returns consumption for given month
     
### Extra CSV functionality 
Application reads CSV files when they are  placed in a given folder (csv.upload.dir parameter in application.properties) 
and process them as data inputs


####Test data for profiles: 
    [
      {"name":"A", "month":"JAN", "fraction":"0.082"},
      {"name":"A", "month":"FEB", "fraction":"0.082"},
      {"name":"A", "month":"MAR", "fraction":"0.082"},
      {"name":"A", "month":"APR", "fraction":"0.082"},
      {"name":"A", "month":"MAY", "fraction":"0.082"},
      {"name":"A", "month":"JUN", "fraction":"0.082"},
      {"name":"A", "month":"JUL", "fraction":"0.082"},
      {"name":"A", "month":"AUG", "fraction":"0.082"},
      {"name":"A", "month":"SEP", "fraction":"0.082"},
      {"name":"A", "month":"OCT", "fraction":"0.082"},
      {"name":"A", "month":"NOV", "fraction":"0.09"},
      {"name":"A", "month":"DEC", "fraction":"0.09"}
    ]
    
   
   