package net.lafox.demo.power.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@Builder
@EqualsAndHashCode
public class ProfileDto {

    private String name;
    private String month;
    private Double fraction;

}
