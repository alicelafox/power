package net.lafox.demo.power.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class MeterReadingDto {

    private String profileName;
    private String month;
    private String connectionId;
    private Integer value;

}
