package net.lafox.demo.power.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@Builder
@EqualsAndHashCode
public class ConsumptionDto {
    String connectionId;
    String monthName;
    String profileName;
    Double consumption;
}
