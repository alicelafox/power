package net.lafox.demo.power.api;

import net.lafox.demo.power.service.ConsumptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/consumption")
public class ConsumptionController {

    @Autowired
    ConsumptionService consumptionService;

    @GetMapping(value = "/{month}", produces = "application/json")
    public ResponseEntity getConsumption(@PathVariable String month) {
        return ResponseEntity.ok(consumptionService.getConsumption(month));
    }

}
