package net.lafox.demo.power.api;

import java.util.List;

import net.lafox.demo.power.dto.MeterReadingDto;
import net.lafox.demo.power.service.MeterReadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/meterReading")
public class MeterReadingController {
    @Autowired
    private MeterReadingService meterReadingService;

    @PostMapping(value = "", consumes = "application/json")
    public ResponseEntity create(@RequestBody List<MeterReadingDto> dtoList) {
        meterReadingService.create(dtoList);
        return ResponseEntity.status(HttpStatus.CREATED).body("OK");
    }

    @GetMapping(value = "/{connectionId}", produces = "application/json")
    public ResponseEntity read(@PathVariable String connectionId) {
        return ResponseEntity.ok(meterReadingService.read(connectionId));
    }

    @PutMapping(value = "", consumes = "application/json")
    public ResponseEntity update(@RequestBody List<MeterReadingDto> dtoList) {
        meterReadingService.update(dtoList);
        return ResponseEntity.status(HttpStatus.OK).body("OK");
    }

    @DeleteMapping(value = "/{connectionId}")
    public ResponseEntity delete(@PathVariable String connectionId) {
        meterReadingService.delete(connectionId);
        return ResponseEntity.status(HttpStatus.OK).body("OK");
    }

}
