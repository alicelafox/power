package net.lafox.demo.power.api;

import java.util.List;

import net.lafox.demo.power.dto.ProfileDto;
import net.lafox.demo.power.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/profile")
public class ProfileController {


    @Autowired
    private ProfileService profileService;

    @PostMapping(value = "", consumes = "application/json")
    public ResponseEntity create(@RequestBody List<ProfileDto> profiles) {
        profileService.create(profiles);
        return ResponseEntity.status(HttpStatus.CREATED).body("OK");
    }

    @GetMapping(value = "/{profileName}", produces = "application/json")
    public ResponseEntity read(@PathVariable String profileName) {
        return ResponseEntity.ok(profileService.read(profileName));
    }

    @PutMapping(value = "", consumes = "application/json")
    public ResponseEntity update(@RequestBody List<ProfileDto> profiles) {
        profileService.update(profiles);
        return ResponseEntity.status(HttpStatus.OK).body("OK");
    }

    @DeleteMapping(value = "/{profileName}")
    public ResponseEntity delete(@PathVariable String profileName) {
        profileService.delete(profileName);
        return ResponseEntity.status(HttpStatus.OK).body("OK");
    }

}
