package net.lafox.demo.power.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.SetMultimap;
import net.lafox.demo.power.dto.ConsumptionDto;
import net.lafox.demo.power.dto.MeterReadingDto;
import net.lafox.demo.power.entity.MeterReadingEntity;
import net.lafox.demo.power.entity.ProfileEntity;
import net.lafox.demo.power.enums.Month;

public class MeterReadingUtils {

    public static SetMultimap<String, MeterReadingDto> splitByConnection(Collection<MeterReadingDto> dtoList) {
        SetMultimap<String, MeterReadingDto> map = HashMultimap.create();
        dtoList.forEach(p -> map.put(p.getConnectionId(), p));
        return map;
    }

    public static List<MeterReadingEntity> toEntityList(Collection<MeterReadingDto> dtos, Map<Month, ProfileEntity> profilesMap) {
        return dtos.stream().map(p -> toEntity(p, profilesMap.get(Month.valueOf(p.getMonth())))).collect(Collectors.toList());
    }

    public static MeterReadingEntity toEntity(MeterReadingDto dto, ProfileEntity profile) {
        MeterReadingEntity entity = new MeterReadingEntity();
        entity.setConnectionId(dto.getConnectionId());
        entity.setProfile(profile);
        entity.setValue(dto.getValue());
        return entity;
    }

    public static List<MeterReadingDto> toDtoList(Collection<MeterReadingEntity> entities) {
        return entities.stream().map(p -> toDto(p)).collect(Collectors.toList());
    }

    private static MeterReadingDto toDto(MeterReadingEntity entity) {
        return MeterReadingDto.builder()
                .value(entity.getValue())
                .connectionId(entity.getConnectionId())
                .profileName(entity.getProfile().getName())
                .build();
    }


    public static Map<Month, Double> toConsumptionByMonth(List<MeterReadingDto> list) {
        Map<Month, Double> consumptionMap = Maps.newHashMap();
        double previousValue = 0.0;
        for (Month month : Month.values()) {
            double currentValue = list.get(month.ordinal()).getValue().doubleValue();
            consumptionMap.put(month, currentValue - previousValue);
            previousValue = currentValue;
        }
        return consumptionMap;
    }


    public static SetMultimap<Month, ConsumptionDto> toConsumptionDtoByMonth(List<MeterReadingEntity> list) {
        SetMultimap<Month, ConsumptionDto> consumptionMap = HashMultimap.create();
        double previousValue = 0.0;

        for (Month month : Month.values()) {
            MeterReadingEntity entity = list.get(month.ordinal());
            double currentValue = entity.getValue().doubleValue();
            consumptionMap.put(month, createConsumptionDto(entity, currentValue - previousValue));
            previousValue = currentValue;
        }

        return consumptionMap;
    }

    private static ConsumptionDto createConsumptionDto(MeterReadingEntity entity, double consumptionValue) {
        return ConsumptionDto.builder()
                .connectionId(entity.getConnectionId())
                .consumption(consumptionValue)
                .monthName(entity.getProfile().getMonth().name())
                .profileName(entity.getProfile().getName()).build();
    }




}
