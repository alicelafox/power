package net.lafox.demo.power.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import net.lafox.demo.power.dto.ProfileDto;
import net.lafox.demo.power.entity.ProfileEntity;
import net.lafox.demo.power.enums.Month;

public class ProfileUtils {

    public static List<ProfileEntity> toEntityList(Collection<ProfileDto> dtos) {
        return dtos.stream().map(p -> toEntity(p)).collect(Collectors.toList());
    }

    public static ProfileEntity toEntity(ProfileDto dto) {
        ProfileEntity entity = new ProfileEntity();
        entity.setFraction(dto.getFraction());
        entity.setMonth(Month.valueOf(dto.getMonth()));
        entity.setName(dto.getName());
        return entity;
    }

    public static SetMultimap<String, ProfileDto> splitByProfile(Collection<ProfileDto> profiles) {
        SetMultimap<String, ProfileDto> map = HashMultimap.create();
        profiles.forEach(p -> map.put(p.getName(), p));
        return map;
    }

    public static List<ProfileDto> toDtoList(Collection<ProfileEntity> entities) {
        return entities.stream().map(p -> toDto(p)).collect(Collectors.toList());
    }

    public static ProfileDto toDto(ProfileEntity entity) {
        return ProfileDto.builder()
                .name(entity.getName())
                .fraction(entity.getFraction())
                .month(entity.getMonth().name())
                .build();
    }

    public static Map<Month, ProfileEntity> getProfilesByMonth(List<ProfileEntity> profiles) {
        return profiles.stream().collect(Collectors.toMap(p -> p.getMonth(), p -> p));
    }
}