package net.lafox.demo.power.exception;

public class ConsumptionException extends RuntimeException {
    public ConsumptionException() {
    }

    public ConsumptionException(String s) {
        super(s);
    }

    public ConsumptionException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
