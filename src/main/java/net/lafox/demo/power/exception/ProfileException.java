package net.lafox.demo.power.exception;

public class ProfileException extends RuntimeException {

    public ProfileException(String s) {
        super(s);
    }

    public ProfileException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ProfileException(Throwable throwable) {
        super(throwable);
    }
}
