package net.lafox.demo.power.exception;

public class MeterReadingException extends RuntimeException {
    public MeterReadingException() {
        super();
    }

    public MeterReadingException(String s) {
        super(s);
    }

    public MeterReadingException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
