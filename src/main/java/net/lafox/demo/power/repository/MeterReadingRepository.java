package net.lafox.demo.power.repository;

import java.util.List;

import net.lafox.demo.power.entity.MeterReadingEntity;
import net.lafox.demo.power.enums.Month;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface MeterReadingRepository extends CrudRepository<MeterReadingEntity, Integer> {
    List<MeterReadingEntity> findAllByConnectionId(String connectionId);

    @Query("select e from MeterReadingEntity e where e.profile.month =:month")
    List<MeterReadingEntity> findAllByProfileMonth(@Param("month") Month month);
}
