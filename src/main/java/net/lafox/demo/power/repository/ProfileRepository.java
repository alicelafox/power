package net.lafox.demo.power.repository;

import java.util.List;

import net.lafox.demo.power.entity.ProfileEntity;
import org.springframework.data.repository.CrudRepository;

public interface ProfileRepository extends CrudRepository<ProfileEntity, Integer> {
    List<ProfileEntity> findAllByName(String name);
}
