package net.lafox.demo.power.validator;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import net.lafox.demo.power.dto.MeterReadingDto;
import net.lafox.demo.power.entity.ProfileEntity;
import net.lafox.demo.power.enums.Month;
import net.lafox.demo.power.exception.MeterReadingException;
import net.lafox.demo.power.util.MeterReadingUtils;
import org.springframework.util.CollectionUtils;

public class MeterReadingValidator {

    public static void validate(Collection<MeterReadingDto> dtoSet, Map<Month, ProfileEntity> profileEntityMap, double acceptableTolerance) {
        checkProfilesDataExists(profileEntityMap);
        checkDataProvided(dtoSet);
        checkHas12Month(dtoSet);

        List<MeterReadingDto> sortedByMonth = getSortedData(dtoSet);
        checkMonthMeterReadingIncremented(sortedByMonth);
        checkConsumption(sortedByMonth, profileEntityMap, acceptableTolerance);

    }

    private static void checkDataProvided(Collection<MeterReadingDto> dtoSet) {
        if (CollectionUtils.isEmpty(dtoSet)) throw new MeterReadingException("No data provided");
    }

    private static void checkProfilesDataExists(Map<Month, ProfileEntity> profileEntityMap) {
        if (CollectionUtils.isEmpty(profileEntityMap))
            throw new MeterReadingException("No profiles data provided");
    }


    private static void checkHas12Month(Collection<MeterReadingDto> dtoSet) {
        if (dtoSet.size() != 12) throw new MeterReadingException("Profile must have 12 months data");

        Set<String> profileMonths = dtoSet.stream().map(p -> p.getMonth()).collect(Collectors.toSet());

        if (profileMonths.size() != 12) throw new MeterReadingException("Profile must have 12 months data");

        for (Month month : Month.values()) {
            profileMonths.remove(month.name());
        }

        if (!CollectionUtils.isEmpty(profileMonths))
            throw new MeterReadingException("Profile must have 12 months data");

    }

    private static void checkMonthMeterReadingIncremented(List<MeterReadingDto> list) {
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i).getValue() < list.get(i - 1).getValue()) {
                throw new MeterReadingException("Meter Reading in " + list.get(i).getMonth() + " is less that in " + list.get(i - 1).getMonth());
            }
        }
    }

    private static List<MeterReadingDto> getSortedData(Collection<MeterReadingDto> dtoSet) {
        List<MeterReadingDto> list = Lists.newArrayList(dtoSet);
        list.sort(Comparator.comparing(m -> Month.valueOf(m.getMonth())));
        return list;
    }

    private static void checkConsumption(List<MeterReadingDto> meterReadingDtos, Map<Month, ProfileEntity> profilesByMonth, double acceptableTolerance) {
        Map<Month, Double> monthlyConsumptions = MeterReadingUtils.toConsumptionByMonth(meterReadingDtos);
        Double totalConsumption = monthlyConsumptions.values().stream().mapToDouble(Double::doubleValue).sum();

        for (Month month : Month.values()) {
            double fraction = profilesByMonth.get(month).getFraction();
            double acceptableConsumption = totalConsumption * fraction;
            double tolerance = acceptableConsumption * acceptableTolerance;

            Range<Double> range = Range.closed(acceptableConsumption - tolerance, acceptableConsumption + tolerance);

            if (!range.contains(monthlyConsumptions.get(month)))
                throw new MeterReadingException("Consumption exceeded tolerance");
        }

    }


}
