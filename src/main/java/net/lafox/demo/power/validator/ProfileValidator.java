package net.lafox.demo.power.validator;

import java.util.Set;
import java.util.stream.Collectors;

import net.lafox.demo.power.dto.ProfileDto;
import net.lafox.demo.power.enums.Month;
import net.lafox.demo.power.exception.ProfileException;
import org.springframework.util.CollectionUtils;


public class ProfileValidator {

    public static void validate(Set<ProfileDto> profileDtos) {
        checkIsNotEmpty(profileDtos);
        checkHas12Month(profileDtos);
        checkHasOnlyValidMonths(profileDtos);
        checkFractionsSum(profileDtos);
    }

    private static void checkFractionsSum(Set<ProfileDto> profileDtos) {
        double sum = profileDtos.stream().mapToDouble(p -> p.getFraction()).sum();
        if (sum > 1.001 || sum < 0.999) throw new ProfileException("Profile fraction sum is not 1, actual sum: " + sum);
    }

    private static void checkHas12Month(Set<ProfileDto> profileDtos) {
        Set<String> profileMonths = profileDtos.stream().map(p -> p.getMonth()).collect(Collectors.toSet());
        for (Month month : Month.values()) {
            if (!profileMonths.contains(month.toString()))
                throw new ProfileException("Profile must have 12 months data");
        }
    }

    private static void checkHasOnlyValidMonths(Set<ProfileDto> profileDtos) {
        profileDtos.stream().map(p -> p.getMonth()).forEach(p -> {
            try {
                Month.valueOf(p);
            } catch (IllegalArgumentException e) {
                throw new ProfileException("Incorrect Month detected: " + p);
            }
        });
    }

    private static void checkIsNotEmpty(Set<ProfileDto> profileDtos) {
        if (CollectionUtils.isEmpty(profileDtos)) throw new ProfileException("No data provided");
    }


}
