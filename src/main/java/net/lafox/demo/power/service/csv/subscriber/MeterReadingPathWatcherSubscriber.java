package net.lafox.demo.power.service.csv.subscriber;

import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import net.lafox.demo.power.dto.MeterReadingDto;
import net.lafox.demo.power.service.MeterReadingService;
import net.lafox.demo.power.service.csv.pathwatcher.PathWatcherEventType;
import net.lafox.demo.power.service.csv.pathwatcher.PathWatcherSubscriber;
import net.lafox.demo.power.service.csv.pathwatcher.PathWatcherUtil;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/*
ConnectionID,Profile,Month,Meter reading
0001,A,JAN,10
0004,B,JAN,8
0004,B,FEB,10
0001,A,FEB,12
0001,A,MAR,18
*/

@Service
public class MeterReadingPathWatcherSubscriber implements PathWatcherSubscriber {
    private static final Logger LOG = LoggerFactory.getLogger(MeterReadingPathWatcherSubscriber.class);

    @Autowired
    private MeterReadingService meterReadingService;

    @Value("${csv.meterreading.filename.prefix:null}")
    private String validFileNamePrefix;

    @Override
    public void handler(PathWatcherEventType pathWatcherEventType, Path path) {
        if (PathWatcherUtil.isNotValidFileName(path, validFileNamePrefix)) return;

        LOG.info("Reading MeterReading CSV file:" + path);
        try {
            CSVParser parser = new CSVParser(new FileReader(path.toFile()), CSVFormat.DEFAULT.withHeader());

            List<MeterReadingDto> dtoList =parser.getRecords().stream().map(this::parseRecord).collect(Collectors.toList());

            meterReadingService.create(dtoList);

            Files.delete(path);

        } catch (Exception e) {
            LOG.error("Error reading CSV file:" + path, e);
        }

    }

    private MeterReadingDto parseRecord(CSVRecord csvRecord) {
        return MeterReadingDto.builder()
                .connectionId(csvRecord.get("ConnectionID"))
                .profileName(csvRecord.get("Profile"))
                .month(csvRecord.get("Month"))
                .value(NumberUtils.toInt(csvRecord.get("Meter reading")))
                .build();
    }

}
