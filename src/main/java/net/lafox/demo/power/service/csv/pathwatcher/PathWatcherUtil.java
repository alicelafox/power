package net.lafox.demo.power.service.csv.pathwatcher;

import java.nio.file.Path;

public class PathWatcherUtil {

    public static boolean isNotValidFileName(Path path, String validFileNamePrefix) {
        String fileName = path.getFileName().toString();
        return !(fileName.startsWith(validFileNamePrefix) && fileName.endsWith(".csv"));
    }

}
