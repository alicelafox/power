package net.lafox.demo.power.service;

import java.util.Collection;
import java.util.List;

import net.lafox.demo.power.dto.MeterReadingDto;

public interface MeterReadingService {

    void create(Collection<MeterReadingDto> dtoList);

    List<MeterReadingDto> read(String connectionId);

    void update(Collection<MeterReadingDto> dtoList);

    void delete(String connectionId);
}
