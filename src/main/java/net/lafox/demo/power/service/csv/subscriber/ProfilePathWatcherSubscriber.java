package net.lafox.demo.power.service.csv.subscriber;

import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import net.lafox.demo.power.dto.ProfileDto;
import net.lafox.demo.power.service.ProfileService;
import net.lafox.demo.power.service.csv.pathwatcher.PathWatcherEventType;
import net.lafox.demo.power.service.csv.pathwatcher.PathWatcherSubscriber;
import net.lafox.demo.power.service.csv.pathwatcher.PathWatcherUtil;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/*
Month,Profile,Fraction
JAN,A,0.2
JAN,B,0.18
FEB,A,0.1
FEB,B,0.21
*/

@Service
public class ProfilePathWatcherSubscriber implements PathWatcherSubscriber {
    private static final Logger LOG = LoggerFactory.getLogger(ProfilePathWatcherSubscriber.class);

    @Autowired
    private ProfileService profileService;

    @Value("${csv.profile.filename.prefix:null}")
    private String validFileNamePrefix;

    @Override
    public void handler(PathWatcherEventType pathWatcherEventType, Path path) {
        if (PathWatcherUtil.isNotValidFileName(path, validFileNamePrefix)) return;

        LOG.info("Reading Profile CSV file:" + path);
        try {
            CSVParser parser = new CSVParser(new FileReader(path.toFile()), CSVFormat.DEFAULT.withHeader());

            List<ProfileDto> dtoList = parser.getRecords().stream().map(this::parseRecord).collect(Collectors.toList());
            profileService.create(dtoList);
            Files.delete(path);

        } catch (Exception e) {
            LOG.error("Error reading CSV file:" + path, e);
        }

    }

    private ProfileDto parseRecord(CSVRecord csvRecord) {
        return ProfileDto.builder()
                .month(csvRecord.get("Month"))
                .name(csvRecord.get("Profile"))
                .fraction(NumberUtils.toDouble(csvRecord.get("Fraction")))
                .build();
    }

}
