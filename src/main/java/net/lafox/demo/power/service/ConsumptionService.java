package net.lafox.demo.power.service;

import java.util.Set;

import net.lafox.demo.power.dto.ConsumptionDto;

public interface ConsumptionService {

    Set<ConsumptionDto> getConsumption(String month);

}
