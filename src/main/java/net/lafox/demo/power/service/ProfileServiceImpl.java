package net.lafox.demo.power.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.transaction.Transactional;

import com.google.common.collect.SetMultimap;
import net.lafox.demo.power.dto.ProfileDto;
import net.lafox.demo.power.exception.ProfileException;
import net.lafox.demo.power.repository.ProfileRepository;
import net.lafox.demo.power.util.ProfileUtils;
import net.lafox.demo.power.validator.ProfileValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProfileServiceImpl implements ProfileService {
    private static final Logger LOG = LoggerFactory.getLogger(ProfileServiceImpl.class);

    @Autowired
    private ProfileRepository profileRepository;


    @Override
    public void create(Collection<ProfileDto> profiles) {
        SetMultimap<String, ProfileDto> splittedByProfile = ProfileUtils.splitByProfile(profiles);
        splittedByProfile.keySet().stream().map(splittedByProfile::get).forEach(this::saveProfile);
    }

    @Override
    public List<ProfileDto> read(String profileName) {
        return ProfileUtils.toDtoList(profileRepository.findAllByName(profileName));
    }

    @Override
    public void update(Collection<ProfileDto> profiles) {
        throw new NotImplementedException("Update requires more information to be implemented");
    }

    @Override
    public void delete(String profileName) {
        profileRepository.delete(profileRepository.findAllByName(profileName));
    }

    private void saveProfile(Set<ProfileDto> profileDtoSet) {
        checkProfileDataExistInDatabase(profileDtoSet);
        try {
            ProfileValidator.validate(profileDtoSet);
            profileRepository.save(ProfileUtils.toEntityList(profileDtoSet));
        } catch (ProfileException e) {
            LOG.warn(e.getMessage());
        }
    }

    private void checkProfileDataExistInDatabase(Set<ProfileDto> profileDtoSet) {
        String profileName = getProfileName(profileDtoSet);
        if (CollectionUtils.isNotEmpty(profileRepository.findAllByName(profileName))) {
            String msg = "Profile can not be created, already exists. Please delete profile " + profileName;
            LOG.error(msg);
            throw new ProfileException(msg);
        }
    }

    private String getProfileName(Collection<ProfileDto> dtos) {
        return dtos.stream().map(p -> p.getName()).findFirst().get();
    }


}
