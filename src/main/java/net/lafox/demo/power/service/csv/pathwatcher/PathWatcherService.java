package net.lafox.demo.power.service.csv.pathwatcher;

import javax.annotation.PostConstruct;

import net.lafox.demo.power.service.csv.subscriber.MeterReadingPathWatcherSubscriber;
import net.lafox.demo.power.service.csv.subscriber.ProfilePathWatcherSubscriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PathWatcherService {

    @Autowired
    PathWatcher pathWatcher;
    @Autowired
    ProfilePathWatcherSubscriber profilePathWatcherSubscriber;
    @Autowired
    MeterReadingPathWatcherSubscriber meterReadingPathWatcherSubscriber;


    @PostConstruct
    void init() {
        pathWatcher.subscribe(PathWatcherEventType.ENTRY_CREATE, meterReadingPathWatcherSubscriber);
        pathWatcher.subscribe(PathWatcherEventType.ENTRY_CREATE, profilePathWatcherSubscriber);
        pathWatcher.checkExisted();
        pathWatcher.start();
    }
}
