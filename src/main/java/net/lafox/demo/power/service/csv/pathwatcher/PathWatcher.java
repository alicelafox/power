package net.lafox.demo.power.service.csv.pathwatcher;

public interface PathWatcher {
    void subscribe(PathWatcherEventType watchEventKid, PathWatcherSubscriber subscriber);

    void unSubscribe(PathWatcherEventType watchEventKid, PathWatcherSubscriber subscriber);

    boolean isStarted();

    void checkExisted();

    void start();
}
