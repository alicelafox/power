package net.lafox.demo.power.service.csv.pathwatcher;

public enum PathWatcherEventType {
    ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY
}
