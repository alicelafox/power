package net.lafox.demo.power.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;

import com.google.common.collect.SetMultimap;
import net.lafox.demo.power.dto.MeterReadingDto;
import net.lafox.demo.power.entity.ProfileEntity;
import net.lafox.demo.power.enums.Month;
import net.lafox.demo.power.exception.MeterReadingException;
import net.lafox.demo.power.exception.ProfileException;
import net.lafox.demo.power.repository.MeterReadingRepository;
import net.lafox.demo.power.repository.ProfileRepository;
import net.lafox.demo.power.util.MeterReadingUtils;
import net.lafox.demo.power.util.ProfileUtils;
import net.lafox.demo.power.validator.MeterReadingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class MeterReadingServiceImpl implements MeterReadingService {
    private static final Logger LOG = LoggerFactory.getLogger(MeterReadingServiceImpl.class);

    @Autowired
    private MeterReadingRepository meterReadingRepository;

    @Autowired
    private ProfileRepository profileRepository;

    private double acceptableTolerance;

    @Value("${acceptable.tolerance:0.25}")
    public void setAcceptableTolerance(String value) {
        acceptableTolerance = NumberUtils.toDouble(value, 0.25);
    }

    @Override
    public void create(Collection<MeterReadingDto> dtoList) {
        SetMultimap<String, MeterReadingDto> splittedByConnection = MeterReadingUtils.splitByConnection(dtoList);
        splittedByConnection.keySet().forEach(connectionId -> saveMeterReading(splittedByConnection.get(connectionId), connectionId));
    }

    private void saveMeterReading(Collection<MeterReadingDto> dtoSet, String connectionId) {
        checkDataNotExistInDatabase(connectionId);

        Map<Month, ProfileEntity> profilesMap = ProfileUtils.getProfilesByMonth(profileRepository.findAllByName(getProfileName(dtoSet)));

        try {
            MeterReadingValidator.validate(dtoSet, profilesMap, acceptableTolerance);
            meterReadingRepository.save(MeterReadingUtils.toEntityList(dtoSet, profilesMap));
        } catch (MeterReadingException e) {
            LOG.warn(e.getMessage());
        }
    }



    private String getProfileName(Collection<MeterReadingDto> dtos) {
        return dtos.stream().map(p -> p.getProfileName()).findFirst().get();
    }

    private void checkDataNotExistInDatabase(String connectionId) {
        if (CollectionUtils.isNotEmpty(meterReadingRepository.findAllByConnectionId(connectionId))) {
            String msg = "Meter Readings set can not be created, already exists. Please delete first " + connectionId;
            LOG.error(msg);
            throw new ProfileException(msg);
        }
    }

    @Override
    public List<MeterReadingDto> read(String connectionId) {
        return MeterReadingUtils.toDtoList(meterReadingRepository.findAllByConnectionId(connectionId));
    }

    @Override
    public void update(Collection<MeterReadingDto> dtoList) {
        throw new NotImplementedException("Not implemented yet");
    }

    @Override
    public void delete(String connectionId) {
        meterReadingRepository.delete(meterReadingRepository.findAllByConnectionId(connectionId));
    }


}
