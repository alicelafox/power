package net.lafox.demo.power.service;

import java.util.Collection;
import java.util.List;

import net.lafox.demo.power.dto.ProfileDto;


public interface ProfileService {

    void create(Collection<ProfileDto> profiles);

    List<ProfileDto> read(String profileName);

    void update(Collection<ProfileDto> profiles);

    void delete(String profileName);


}
