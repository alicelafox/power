package net.lafox.demo.power.service;

import java.util.List;
import java.util.Set;

import com.google.common.collect.SetMultimap;
import net.lafox.demo.power.dto.ConsumptionDto;
import net.lafox.demo.power.entity.MeterReadingEntity;
import net.lafox.demo.power.enums.Month;
import net.lafox.demo.power.exception.ConsumptionException;
import net.lafox.demo.power.repository.MeterReadingRepository;
import net.lafox.demo.power.util.MeterReadingUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConsumptionServiceImpl implements ConsumptionService {

    @Autowired
    private MeterReadingRepository meterReadingRepository;

    @Override
    public Set<ConsumptionDto> getConsumption(String monthName) {
        Month month = getMonth(monthName);

        List<MeterReadingEntity> allByProfileMonth = meterReadingRepository.findAllByProfileMonth(month);
        if (CollectionUtils.isEmpty(allByProfileMonth))
            throw new ConsumptionException("No data available");

        SetMultimap<Month, ConsumptionDto> monthConsumption = MeterReadingUtils.toConsumptionDtoByMonth(allByProfileMonth);
        return monthConsumption.get(month);
    }

    private Month getMonth(String monthName) {
        try {
            return Month.valueOf(monthName);
        } catch (Exception e) {
            throw new ConsumptionException("Invalid Month");
        }
    }
}
