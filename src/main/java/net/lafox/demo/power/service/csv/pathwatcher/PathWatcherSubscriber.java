package net.lafox.demo.power.service.csv.pathwatcher;

import java.nio.file.Path;

public interface PathWatcherSubscriber {
    void handler(PathWatcherEventType pathWatcherEventType, Path path);
}
