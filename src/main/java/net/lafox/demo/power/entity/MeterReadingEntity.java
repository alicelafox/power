package net.lafox.demo.power.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "meter_readings")
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class MeterReadingEntity {

    @Id
    @GeneratedValue
    private Integer id;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    private ProfileEntity profile;

    @Column(nullable = false)
    private String connectionId;

    @Column(nullable = false)
    private Integer value;
}
