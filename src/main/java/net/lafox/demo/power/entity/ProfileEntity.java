package net.lafox.demo.power.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.lafox.demo.power.enums.Month;

@Entity
@Table(name = "profiles",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"name", "month"})
        }
)
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class ProfileEntity implements Serializable {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Month month;

    @Column(nullable = false)
    private Double fraction;

}
