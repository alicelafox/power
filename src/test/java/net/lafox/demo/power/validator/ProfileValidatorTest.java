package net.lafox.demo.power.validator;

import net.lafox.demo.power.ProfileTestUtil;
import net.lafox.demo.power.exception.ProfileException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ProfileValidatorTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void thatGoodProfileWorksFine() throws Exception {
        ProfileValidator.validate(ProfileTestUtil.createProfileA());
    }

    @Test
    public void thatProfileWithLessThen12MonthThrowsException() throws Exception {
        expectedEx.expect(ProfileException.class);
        expectedEx.expectMessage("Profile must have 12 months data");
        ProfileValidator.validate(ProfileTestUtil.createProfileLessThen12Month());
    }

    @Test
    public void thatProfileWithWrongMonthThrowsException() throws Exception {
        expectedEx.expect(ProfileException.class);
        expectedEx.expectMessage("Profile must have 12 months data");
        ProfileValidator.validate(ProfileTestUtil.createProfileWithWrongMonth());
    }

    @Test
    public void thatProfileWithWrongMonthAndCountThrowsException() throws Exception {
        expectedEx.expect(ProfileException.class);
        expectedEx.expectMessage("Incorrect Month detected: WRONG MONTH");
        ProfileValidator.validate(ProfileTestUtil.createProfileWithWrongMonthAndCount());
    }

}