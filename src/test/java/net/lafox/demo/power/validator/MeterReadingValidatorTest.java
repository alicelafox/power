package net.lafox.demo.power.validator;

import java.util.Map;

import net.lafox.demo.power.MeterReadingTestUtil;
import net.lafox.demo.power.ProfileTestUtil;
import net.lafox.demo.power.entity.ProfileEntity;
import net.lafox.demo.power.enums.Month;
import net.lafox.demo.power.exception.MeterReadingException;
import net.lafox.demo.power.util.ProfileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class MeterReadingValidatorTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    private Map<Month, ProfileEntity> profilesA, profilesB;
    private double acceptableTolerance = 0.25;

    @Before
    public void setUp() throws Exception {
        profilesA = ProfileUtils.getProfilesByMonth(ProfileUtils.toEntityList(ProfileTestUtil.createProfileA()));
        profilesB = ProfileUtils.getProfilesByMonth(ProfileUtils.toEntityList(ProfileTestUtil.createProfileB()));
    }

    @Test
    public void thatValidateEmptyDataThrowsException() throws Exception {
        expectedEx.expect(MeterReadingException.class);
        MeterReadingValidator.validate(null, null, acceptableTolerance);
    }

    @Test
    public void thatMeterReadingsWithLessThen12MonthThrowsException() throws Exception {
        expectedEx.expect(MeterReadingException.class);
        expectedEx.expectMessage("Profile must have 12 months data");
        MeterReadingValidator.validate(MeterReadingTestUtil.createDataLessThen12Month(), profilesA, acceptableTolerance);
    }

    @Test
    public void thatMeterReadingsWithReversedDataThrowsException() throws Exception {
        expectedEx.expect(MeterReadingException.class);
        expectedEx.expectMessage("Meter Reading in FEB is less that in JAN");
        MeterReadingValidator.validate(MeterReadingTestUtil.createMeterReadingsWithReversedData(), profilesA, acceptableTolerance);
    }

    @Test
    public void thatValidateIncorrectConsumptionThrowsException() throws Exception {
        expectedEx.expect(MeterReadingException.class);
        MeterReadingValidator.validate(MeterReadingTestUtil.createMeterReadings(), profilesA, acceptableTolerance);
    }

    @Test
    public void thatValidateMonthlyConsumptionsWorksCorrectly() throws Exception {
        MeterReadingValidator.validate(MeterReadingTestUtil.createMeterReadings(), profilesB, acceptableTolerance);
    }

}