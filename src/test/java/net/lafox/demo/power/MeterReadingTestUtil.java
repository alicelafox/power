package net.lafox.demo.power;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import net.lafox.demo.power.dto.MeterReadingDto;
import net.lafox.demo.power.enums.Month;

public class MeterReadingTestUtil {
    public static final String CONNECTION_1 = "0001";

    public static List<MeterReadingDto> createMeterReadings() {
        return Arrays.stream(Month.values()).map(m -> createMeterReadingDto(m, (m.ordinal() + 1) * 10)).collect(Collectors.toList());
    }

    public static List<MeterReadingDto> createMeterReadingsWithReversedData() {
        return Arrays.stream(Month.values()).map(m -> createMeterReadingDto(m, (12 - m.ordinal()) * 10)).collect(Collectors.toList());
    }

    private static MeterReadingDto createMeterReadingDto(Month m, Integer value) {
        return MeterReadingDto.builder()
                .connectionId(CONNECTION_1)
                .month(m.toString())
                .profileName(ProfileTestUtil.PROFILE_A)
                .value(value)
                .build();
    }

    public static List<MeterReadingDto> createDataLessThen12Month() {
        return Arrays.stream(Month.values()).filter(m -> m.ordinal() < 5).map(m -> createMeterReadingDto(m, (m.ordinal() + 1) * 10)).collect(Collectors.toList());
    }

}
