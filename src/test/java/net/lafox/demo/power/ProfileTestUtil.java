package net.lafox.demo.power;

import java.util.Set;

import com.google.common.collect.Sets;
import net.lafox.demo.power.dto.ProfileDto;
import net.lafox.demo.power.enums.Month;

public class ProfileTestUtil {

    public static final String PROFILE_A = "A";
    public static final String PROFILE_B = "B";
    public static final String PROFILE_C = "C";

    public static final ProfileDto profileJanA = ProfileDto.builder().fraction(0.1).month(Month.JAN.name()).name(PROFILE_A).build();
    public static final ProfileDto profileFebA = ProfileDto.builder().fraction(0.1).month(Month.FEB.name()).name(PROFILE_A).build();
    public static final ProfileDto profileMarA = ProfileDto.builder().fraction(0.1).month(Month.MAR.name()).name(PROFILE_A).build();
    public static final ProfileDto profileAprA = ProfileDto.builder().fraction(0.1).month(Month.APR.name()).name(PROFILE_A).build();
    public static final ProfileDto profileMayA = ProfileDto.builder().fraction(0.1).month(Month.MAY.name()).name(PROFILE_A).build();
    public static final ProfileDto profileJunA = ProfileDto.builder().fraction(0.1).month(Month.JUN.name()).name(PROFILE_A).build();
    public static final ProfileDto profileJulA = ProfileDto.builder().fraction(0.1).month(Month.JUL.name()).name(PROFILE_A).build();
    public static final ProfileDto profileAugA = ProfileDto.builder().fraction(0.1).month(Month.AUG.name()).name(PROFILE_A).build();
    public static final ProfileDto profileSepA = ProfileDto.builder().fraction(0.05).month(Month.SEP.name()).name(PROFILE_A).build();
    public static final ProfileDto profileOctA = ProfileDto.builder().fraction(0.05).month(Month.OCT.name()).name(PROFILE_A).build();
    public static final ProfileDto profileNovA = ProfileDto.builder().fraction(0.05).month(Month.NOV.name()).name(PROFILE_A).build();
    public static final ProfileDto profileDecA = ProfileDto.builder().fraction(0.05).month(Month.DEC.name()).name(PROFILE_A).build();

    public static final ProfileDto profileJanB = ProfileDto.builder().fraction(0.082).month(Month.JAN.name()).name(PROFILE_B).build();
    public static final ProfileDto profileFebB = ProfileDto.builder().fraction(0.082).month(Month.FEB.name()).name(PROFILE_B).build();
    public static final ProfileDto profileMarB = ProfileDto.builder().fraction(0.082).month(Month.MAR.name()).name(PROFILE_B).build();
    public static final ProfileDto profileAprB = ProfileDto.builder().fraction(0.082).month(Month.APR.name()).name(PROFILE_B).build();
    public static final ProfileDto profileMayB = ProfileDto.builder().fraction(0.082).month(Month.MAY.name()).name(PROFILE_B).build();
    public static final ProfileDto profileJunB = ProfileDto.builder().fraction(0.082).month(Month.JUN.name()).name(PROFILE_B).build();
    public static final ProfileDto profileJulB = ProfileDto.builder().fraction(0.082).month(Month.JUL.name()).name(PROFILE_B).build();
    public static final ProfileDto profileAugB = ProfileDto.builder().fraction(0.082).month(Month.AUG.name()).name(PROFILE_B).build();
    public static final ProfileDto profileSepB = ProfileDto.builder().fraction(0.082).month(Month.SEP.name()).name(PROFILE_B).build();
    public static final ProfileDto profileOctB = ProfileDto.builder().fraction(0.082).month(Month.OCT.name()).name(PROFILE_B).build();
    public static final ProfileDto profileNovB = ProfileDto.builder().fraction(0.090).month(Month.NOV.name()).name(PROFILE_B).build();
    public static final ProfileDto profileDecB = ProfileDto.builder().fraction(0.090).month(Month.DEC.name()).name(PROFILE_B).build();

    public static final ProfileDto profileJanC = ProfileDto.builder().fraction(0.1).month(Month.JAN.name()).name(PROFILE_C).build();
    public static final ProfileDto profileFebC = ProfileDto.builder().fraction(0.1).month(Month.FEB.name()).name(PROFILE_C).build();
    public static final ProfileDto profileMarC = ProfileDto.builder().fraction(0.1).month(Month.MAR.name()).name(PROFILE_C).build();
    public static final ProfileDto profileAprC = ProfileDto.builder().fraction(0.1).month(Month.APR.name()).name(PROFILE_C).build();
    public static final ProfileDto profileMayC = ProfileDto.builder().fraction(0.1).month(Month.MAY.name()).name(PROFILE_C).build();
    public static final ProfileDto profileJunC = ProfileDto.builder().fraction(0.1).month(Month.JUN.name()).name(PROFILE_C).build();
    public static final ProfileDto profileJulC = ProfileDto.builder().fraction(0.1).month(Month.JUL.name()).name(PROFILE_C).build();
    public static final ProfileDto profileAugC = ProfileDto.builder().fraction(0.1).month(Month.AUG.name()).name(PROFILE_C).build();
    public static final ProfileDto profileSepC = ProfileDto.builder().fraction(0.05).month(Month.SEP.name()).name(PROFILE_C).build();
    public static final ProfileDto profileOctC = ProfileDto.builder().fraction(0.05).month(Month.OCT.name()).name(PROFILE_C).build();
    public static final ProfileDto profileNovC = ProfileDto.builder().fraction(0.05).month(Month.NOV.name()).name(PROFILE_C).build();
    public static final ProfileDto profileDecC = ProfileDto.builder().fraction(0.05).month(Month.DEC.name()).name(PROFILE_C).build();

    public static Set<ProfileDto> createProfileA() {
        return Sets.newHashSet(profileJanA, profileFebA, profileMarA, profileAprA, profileMayA, profileJunA,
                profileJulA, profileAugA, profileSepA, profileOctA, profileNovA, profileDecA);
    }

    public static Set<ProfileDto> createProfileB() {
        return Sets.newHashSet(profileJanB, profileFebB, profileMarB, profileAprB, profileMayB, profileJunB,
                profileJulB, profileAugB, profileSepB, profileOctB, profileNovB, profileDecB);
    }


    public static Set<ProfileDto> createProfileC() {
        return Sets.newHashSet(profileJanC, profileFebC, profileMarC, profileAprC, profileMayC, profileJunC,
                profileJulC, profileAugC, profileSepC, profileOctC, profileNovC, profileDecC);
    }


    public static Set<ProfileDto> createProfileLessThen12Month() {
        return Sets.newHashSet( // < skipped Jan
                profileFebC, profileMarC, profileAprC, profileMayC, profileJunC,
                profileJulC, profileAugC, profileSepC, profileOctC, profileNovC, profileDecC);
    }

    public static Set<ProfileDto> createProfileWithWrongMonth() {
        return Sets.newHashSet(ProfileDto.builder().fraction(0.1).month("WRONG MONTH").name(PROFILE_C).build(), // < wrong Month
                profileFebC, profileMarC, profileAprC, profileMayC, profileJunC,
                profileJulC, profileAugC, profileSepC, profileOctC, profileNovC, profileDecC);
    }

    public static Set<ProfileDto> createProfileWithWrongMonthAndCount() {
        return Sets.newHashSet(profileJanC, ProfileDto.builder().fraction(0.0).month("WRONG MONTH").name(PROFILE_C).build(), // < wrong Month
                profileFebC, profileMarC, profileAprC, profileMayC, profileJunC,
                profileJulC, profileAugC, profileSepC, profileOctC, profileNovC, profileDecC);
    }

    public static Set<ProfileDto> createProfileCWrongFractionSum1() {
        return Sets.newHashSet(ProfileDto.builder().fraction(0.2).month(Month.DEC.name()).name(PROFILE_C).build(), // < wrong fraction
                profileFebC, profileMarC, profileAprC, profileMayC, profileJunC,
                profileJulC, profileAugC, profileSepC, profileOctC, profileNovC, profileDecC);
    }

    public static Set<ProfileDto> createProfileCWrongFractionSum2() {
        return Sets.newHashSet(ProfileDto.builder().fraction(-0.2).month(Month.DEC.name()).name(PROFILE_C).build(), // < wrong fraction 2
                profileFebC, profileMarC, profileAprC, profileMayC, profileJunC,
                profileJulC, profileAugC, profileSepC, profileOctC, profileNovC, profileDecC);
    }

}
