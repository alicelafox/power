package net.lafox.demo.power.api;

import static net.lafox.demo.power.ProfileTestUtil.PROFILE_A;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.lafox.demo.power.ProfileTestUtil;
import net.lafox.demo.power.dto.ProfileDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * This (integration) test was created only with one purpose - to make sure that REST API controller works
 * without need to go to another application (like browser or postman) to make requests. I'm too lazy for it :)
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ApiTest {

    private final ObjectMapper mapper = new ObjectMapper();
    private final String API_URL = "/api/v1/profile";
    private Set<ProfileDto> profileDtos;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        profileDtos = ProfileTestUtil.createProfileA();
    }

    @Test
    public void testCreateApiWorks() throws Exception {
        String json = mapper.writeValueAsString(profileDtos);
        mockMvc.perform(post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
        ).andExpect(status().isCreated());
    }

    @Test
    public void testDeleteApiWorks() throws Exception {
        mockMvc.perform(delete(API_URL + "/" + PROFILE_A))
                .andExpect(status().isOk());
    }

}